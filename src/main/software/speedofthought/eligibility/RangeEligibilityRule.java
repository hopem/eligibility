package software.speedofthought.eligibility;

import java.util.function.Predicate;

/**
 * Created by Mark on 02/03/2017.
 */
interface RangeEligibilityRule<T> {

    T getMaximum();

    T getMinimum();

    Predicate<Employee> getEligibilityPredicate(T parameter);

}
