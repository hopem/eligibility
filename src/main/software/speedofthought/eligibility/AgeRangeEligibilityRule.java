package software.speedofthought.eligibility;

/**
 * Created by Mark on 02/03/2017.
 */
abstract class AgeRangeEligibilityRule implements RangeEligibilityRule<Integer> {
    @Override
    public Integer getMaximum() {
        return 99;
    }

    @Override
    public Integer getMinimum() {
        return 1;
    }

}
