package software.speedofthought.eligibility;

/**
 * Created by Mark on 02/03/2017.
 */
class Employee {
    private Integer age;

    Employee(Integer age) {
        this.age = age;
    }

    Integer getAge() {
        return age;
    }
}
