package software.speedofthought.eligibility;

import java.util.function.Predicate;

/**
 * Created by Mark on 02/03/2017.
 */
class AgeMoreThanEligibilityRule extends AgeRangeEligibilityRule {
    @Override
    public Predicate<Employee> getEligibilityPredicate(Integer age) {
        return (employee -> employee.getAge().compareTo(age) > 0);
    }
}
