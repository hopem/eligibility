package software.speedofthought.eligibility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * Created by Mark on 02/03/2017.
 */
@RunWith(JUnit4.class)
public class AgeMoreThanEligibilityRuleTest {

    @Test
    public void employeeOldEnoughShouldBeEligible(){
        Employee employee = new Employee(21);
        Predicate<Employee> employeePredicate = new AgeMoreThanEligibilityRule().getEligibilityPredicate(20);
        assertThat(employeePredicate.test(employee), is(true));
    }

    @Test
    public void employeeTooYoungShouldBeIneligible(){
        Employee employee = new Employee(20);
        Predicate<Employee> employeePredicate = new AgeMoreThanEligibilityRule().getEligibilityPredicate(20);
        assertThat(employeePredicate.test(employee), is(false));
    }

    @Test
    public void shouldGetMaximumValidAge(){
        assertThat(new AgeMoreThanEligibilityRule().getMaximum(), is(99));
    }

    @Test
    public void shouldGetMinimumValidAge(){
        assertThat(new AgeMoreThanEligibilityRule().getMinimum(), is(1));
    }

}